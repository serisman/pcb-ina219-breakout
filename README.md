# INA219 - Current Sensor Breakout v1.0 #

## Schematic ##

![Schematic.png](https://bytebucket.org/serisman/pcb-ina219-breakout/raw/master/output/Schematic.png)

## Design Rules ##

* Clearance: 8 mil
* Track Width: 20 mil
* Via Diameter: 27 mil
* Via Drill: 13 mil
* Zone Clearance: 8 mil
* Zone Min Width: 8 mil
* Zone Thermal Antipad Clearance: 8 mil
* Zone Thermal Spoke Width: 15 mil

## PCB available on OSH Park ##

* 0.83" x 0.65" (21.01 mm x 16.56 mm)
* $0.8833 each ($2.65 for 3)
* [https://oshpark.com/shared_projects/XZri2moE](https://oshpark.com/shared_projects/XZri2moE)

### PCB Front ###

![PCB-Front.png](https://bytebucket.org/serisman/pcb-ina219-breakout/raw/master/output/PCB-Front.png)

### PCB Back ###

![PCB-Back.png](https://bytebucket.org/serisman/pcb-ina219-breakout/raw/master/output/PCB-Back.png)